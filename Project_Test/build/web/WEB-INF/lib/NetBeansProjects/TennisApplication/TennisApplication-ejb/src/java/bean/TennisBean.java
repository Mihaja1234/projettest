/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import connection.Connexion;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.ejb.Stateless;

/**
 *
 * @author ASUS
 */
@Stateless
public class TennisBean implements TennisBeanLocal {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method
      @Override
      public void insertScore(int id,Connexion con) throws SQLException, ClassNotFoundException {
        try {
            PreparedStatement pstat = con.getPreparedStatement("insert into score (id_joueur) values (?)");
            pstat.setInt(1, id);
            int res = pstat.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }
    }
    
}
