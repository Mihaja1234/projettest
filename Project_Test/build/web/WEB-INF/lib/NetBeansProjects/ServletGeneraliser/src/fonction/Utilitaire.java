package fonction;

import Annotation.UrlAnnotation;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import jakarta.servlet.http.HttpServletRequest;
import java.sql.Date;
import mv.ModelView;

public class Utilitaire {

    String transformeMaj(String mot) {

        return mot.substring(0, 1).toUpperCase().concat(mot.substring(1));
    }

    public String[] verificationUrl(String url) {
        String[] valiny = url.split("-");
        return valiny;
    }

    public String retrieveClass(String name) {
        String[] values = name.split("\\.");
        return values[0];
    }

    public String retrieveClass(ArrayList<String> a) {
        String name = a.get(0);
        String[] values = name.split("\\.");
        return values[0].concat(".").concat(values[1]);
    }

    public String retrieveParameterName(String name) {
        String[] values = name.split("\\.");
        return values[2];
    }

    public String MethodAnnoterURL(String url) {
        int urlIndex = url.indexOf("/");
        String methode = url.substring(urlIndex + 1, url.length() - 3);
        return methode;
    }

    public List<String> searchClassNames(String packageName) throws IOException {
        String[] pathnames;
        List<String> result = new ArrayList<String>();
        File f = new File("D:\\Users\\ASUS\\Documents\\NetBeansProjects\\Project_Test\\build\\web\\WEB-INF\\classes\\" + packageName);
        pathnames = f.list();
        for (String pathname : pathnames) {
            pathname = pathname.substring(0, pathname.length() - 6);
            result.add(packageName + "." + pathname);
        }
        return result;
    }

    public HashMap<String, Object> stockage() throws IOException, ClassNotFoundException {

        HashMap<String, Object> result = new HashMap();
        List<String> listeClassesPackage = searchClassNames("controller");
        Class classe = null;
        List<String> listClassCorrespondant = new ArrayList();
        List<Method> listMethodCorrespondant = new ArrayList();
        List<String> listAnnotationCorrespondant = new ArrayList();
        for (String nomClass : listeClassesPackage) {
            classe = Class.forName(nomClass.replace(".class", ""));
            for (Method method : classe.getDeclaredMethods()) {
                if (method.isAnnotationPresent(UrlAnnotation.url.class)) {
                    UrlAnnotation.url annotation = method.getAnnotation(UrlAnnotation.url.class);
                    listClassCorrespondant.add(classe.getSimpleName());
                    listMethodCorrespondant.add(method);
                    listAnnotationCorrespondant.add(annotation.name());
                    result.put("ListeClasse", listClassCorrespondant);
                    result.put("ListeMethod", listMethodCorrespondant);
                    result.put("ListeAnnotation", listAnnotationCorrespondant);
                }
            }
        }
        return result;
    }

    boolean estStocke(String url, HashMap<String, Object> stockage) {
        List annotation = (List) stockage.get("ListeAnnotation");
        for (int i = 0; i < annotation.size(); i++) {
            if (url.equals(annotation.get(i))) {
                return true;
            }
        }
        return false;
    }

    public String checkClassCorrespondant(String annotation, HashMap<String, Object> stockage) throws IOException, ClassNotFoundException {

        List aaa = (List) stockage.get("ListeAnnotation");
        List bbb = (List) stockage.get("ListeClasse");
        String b = " ";
        for (int i = 0; i < aaa.size(); i++) {
            if (annotation.equals(aaa.get(i))) {
                b = bbb.get(i).toString();
                break;
            } else {
                b = "tsy mety";
            }
        }
        return b;
    }

    public Method checkMethodCorrespondant(String annotation, HashMap<String, Object> stockage) throws IOException, ClassNotFoundException {

        List aaa = (List) stockage.get("ListeAnnotation");
        List bbb = (List) stockage.get("ListeMethod");
        Method b = null;
        for (int i = 0; i < aaa.size(); i++) {
            if (annotation.equals(aaa.get(i))) {
                b = (Method) bbb.get(i);
                break;
            } else {
                b = null;
            }
        }
        return b;
    }

    public ArrayList<String> listeParameterName(HttpServletRequest request) {
        Utilitaire t = new Utilitaire();
        Enumeration<String> liste = request.getParameterNames();
        ArrayList<String> result = new ArrayList();
        while (liste.hasMoreElements()) {
            String s = (String) liste.nextElement();
            result.add(t.retrieveParameterName(s));
        }
        return result;
    }

    public ArrayList<String> listeDefaultParameterName(HttpServletRequest request) {
        Utilitaire t = new Utilitaire();
        Enumeration<String> liste = request.getParameterNames();
        ArrayList<String> result = new ArrayList();
        while (liste.hasMoreElements()) {
            String s = (String) liste.nextElement();
            result.add(s);
        }
        return result;
    }

    public ArrayList<String> listeParameterValue(HttpServletRequest request) {
        Enumeration<String> liste = request.getParameterNames();
        ArrayList<String> result = new ArrayList();
        while (liste.hasMoreElements()) {
            String s = (String) liste.nextElement();
            String a = request.getParameter(s).trim();
            result.add(a);
        }
        return result;
    }

    public ArrayList<String> listeAttribute(Object o) {
        Field[] fields = o.getClass().getDeclaredFields();
        ArrayList<String> result = new ArrayList();
        for (int i = 0; i < fields.length; i++) {
            result.add(fields[i].getName());
        }
        return result;
    }
//    ny type anle parametre anle methode Class c

    public Method setMethod(Object o, String name, Class c) {
        Method result = null;
        try {
            name = "set" + transformeMaj(name);
            result = o.getClass().getDeclaredMethod(name, c);
        } catch (NoSuchMethodException | SecurityException ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }

//    public static void main(String[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
//        Personne p = new Personne();
//        Utilitaire u = new Utilitaire();
//        Method m=u.setMethod(p, "name", String.class);
//        m.invoke(p,"4");
//        System.out.println(p.getName());
//    }
    public Object instanceObject(Object o, ArrayList<String> parameterName, ArrayList<String> parameterValue) {
        ArrayList<String> listeAttribute = listeAttribute(o);
        for (int i = 0; i < parameterName.size(); i++) {
            for (String attribute : listeAttribute) {
                if (parameterName.get(i).equals(attribute)) {
                    try {
                        Class<?> classobject = o.getClass().getDeclaredField(attribute).getType();
                        Object value = null;
                        if (classobject.getSimpleName().equals("int")) {
                            value = Integer.parseInt(parameterValue.get(i));
                        }
                        else if (classobject.getSimpleName().equals("Date")) {
                            value = Date.valueOf(parameterValue.get(i));
                        }
                        else{
                            value=parameterValue.get(i);
                        }
                            
                        Method m = setMethod(o, attribute, classobject);
                        m.invoke(o, value);
                    } catch (NoSuchFieldException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        }
        return o;
    }

//    public static void main(String [] args) throws ClassNotFoundException, IOException{
//        Utilitaire t=new Utilitaire();
//        HashMap<String, Object> result = t.stockage();
//        List annotation = (List) result.get("ListeAnnotation");
//        List method = (List) result.get("ListeMethod");
//        List classe = (List) result.get("ListeClasse");
//        
//        for (int i = 0; i < classe.size(); i++) {
//            System.out.print("Class: "+classe.get(i));
//            System.out.print(" || Method: "+method.get(i));
//            System.out.println(" || Url: "+annotation.get(i));
//        }
//    }
//    public static void main(String [] args){
//        Utilitaire t=new Utilitaire();
//        ArrayList<String>name=new ArrayList();
//        name.add("nom");
//        name.add("prenom");
//        ArrayList<String>value=new ArrayList();
//        value.add("hasinjaka1");
//        value.add("hasinjaka");
//        Object o = t.instanceObject(p,name,value);
//        System.out.println(p.getNom());
//        System.out.println(p.getPrenom());
//    }
    public static void main(String[] args) throws IOException, NoSuchFieldException {
        Utilitaire t = new Utilitaire();
        ModelView p = new ModelView();
        ArrayList<String> name = new ArrayList();
        ArrayList<String> value = new ArrayList();
    }
}
