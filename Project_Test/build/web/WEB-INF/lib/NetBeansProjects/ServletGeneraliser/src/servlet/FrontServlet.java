/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import fonction.Utilitaire;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.lang.reflect.*;
import java.util.HashMap;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import mv.ModelView;

public class FrontServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        Utilitaire ts = new Utilitaire();
        ServletContext context = this.getServletContext();
        try {
            context.setAttribute("stockage", ts.stockage());
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = null;
        try {
            Utilitaire ts = new Utilitaire();
            out = response.getWriter();
            ServletContext context = this.getServletContext();
            HashMap<String, Object> stockage = (HashMap<String, Object>) context.getAttribute("stockage");
            int urlIndex = request.getRequestURI().lastIndexOf("/");
            String url = request.getRequestURI().substring(urlIndex + 1);//liste.do
            String methodeAnnoter = ts.MethodAnnoterURL(url);//liste(-do)
            String controllerCorrespondant = ts.checkClassCorrespondant(methodeAnnoter, stockage);//testController
            Class contro = Class.forName("controller." + controllerCorrespondant);//TestController.class
            Method metho = ts.checkMethodCorrespondant(methodeAnnoter, stockage);//liste(ModelView)

            ArrayList<String> name = ts.listeParameterName(request);//liste Parameter Name
            ArrayList<String> value = ts.listeParameterValue(request);// liste Parameter Value

            String inputobjectstring = ts.retrieveClass(ts.listeDefaultParameterName(request));//personne
//            for (int i = 0; i < value.size(); i++) {
//                out.println(name.get(i)+value.get(i));
//                
//            }
            Class inputobjectclass = Class.forName(inputobjectstring); //Personne.class
            Object p = inputobjectclass.newInstance();
            Object o = ts.instanceObject(p, name, value);

            Object ob = contro.newInstance(); // new TestController()
            ModelView mv = (ModelView) metho.invoke(ob); // modelview(liste)
            HashMap hashmap = mv.getData();
            request.setAttribute("object", o);
            request.setAttribute("data", hashmap);
            RequestDispatcher dispa = request.getRequestDispatcher(mv.getPage() + ".jsp");
            dispa.forward(request, response);

        } catch (ClassNotFoundException | IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            out = response.getWriter();
            out.println(ex.getMessage());

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
