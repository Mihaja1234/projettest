/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author ASUS
 */
public class ButtonEcouteur extends JButton{
    public ButtonEcouteur(GenerateFrame j){
        ecouteur(j);
        setSize(150,50);
        
    }
    
    public ButtonEcouteur(GenerateFrame j, int x, int y){
        this(j);
        setLocation(x,y);
        j.add(this);
    }
    public void ecouteur(GenerateFrame j){
        addActionListener((ActionEvent e) -> {
            String text=j.getInput(1).getTextfield().getText();
            System.out.print(text);
        });
    }
}
