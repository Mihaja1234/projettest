/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frame;


import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JTextField;


public class Input {

    JLabel label;
    JTextField textfield;

    public JLabel getLabel() {
        return label;
    }

    public void setLabel(JLabel label) {
        this.label = label;
    }

    public JTextField getTextfield() {
        return textfield;
    }

    public void setTextfield(JTextField textfield) {
        this.textfield = textfield;
    }

    public Input(JFrame f, String title, int x, int y) {
        label = new JLabel(title);
        label.setBounds(x, y, 100, 20);
        textfield = new JTextField();
        textfield.setBounds(x+label.getWidth(), y, 100, 20);
        label.setBackground(java.awt.Color.red);
        f.add(label);
        f.add(textfield);

    }

    public static void main(String args[]) {
        JFrame f = new JFrame();
        f.setBounds(0, 0, 1000, 800);
        new Input(f, "bonjour", 10, 10);
        new Input(f, "bonadfa", 10, 40);
        f.setLayout(null);
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
