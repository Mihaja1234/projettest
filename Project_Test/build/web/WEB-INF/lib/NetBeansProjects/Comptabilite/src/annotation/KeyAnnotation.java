package annotation;
import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
public @interface KeyAnnotation{
    public String column() default "";
}
