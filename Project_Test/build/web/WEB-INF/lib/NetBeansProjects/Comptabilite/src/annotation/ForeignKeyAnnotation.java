package annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
public @interface ForeignKeyAnnotation {
    public String nomTable() default "";
    public String idForeignKey();
    public String PrimaryKey() default "id";
    public String cascade() default "1";
}
