/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comptabilite;

import objet.DBAnnotation;
import objet.ObjectBDD;

/**
 *
 * @author ASUS
 */
public class Balance{

    @DBAnnotation(iscolumn = false,dbname = "")
    private Compte compte;
    
    @DBAnnotation(iscolumn = true, dbname = "solde_initiale")
    private double solde_initiale;
    
    @DBAnnotation(iscolumn = true, dbname = "debit")
    private double debit;
    
    @DBAnnotation(iscolumn = true, dbname = "credit")
    private double credit;
    
    @DBAnnotation(iscolumn = true, dbname = "solde_finale")
    private double solde_finale;
    
    @DBAnnotation(iscolumn = false, dbname = "")
    private boolean est_total;
    
    @DBAnnotation(iscolumn = false, dbname = "")
    private boolean est_grandtotal;

    public static void main(String[] args) {
        Balance b = new Balance();
//        b.getDBColumn(); 

    }
}
