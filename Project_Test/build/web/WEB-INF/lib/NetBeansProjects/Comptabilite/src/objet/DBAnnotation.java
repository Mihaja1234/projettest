/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objet;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author ASUS
 */
@Retention(RetentionPolicy.RUNTIME)

public @interface DBAnnotation {
    boolean iscolumn() default false;
    String dbname();
}
