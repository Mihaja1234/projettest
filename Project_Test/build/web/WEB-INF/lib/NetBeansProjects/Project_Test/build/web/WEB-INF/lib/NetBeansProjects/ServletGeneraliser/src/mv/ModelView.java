
package mv;

import java.util.HashMap;


public class ModelView {

    private HashMap data;
    private String page;

    public ModelView(HashMap m, String page) {
        this.data = m;
        this.page = page;
    }

    public ModelView() {
    }
    
    public HashMap getData() {
        return data;
    }

    public void setData(HashMap data) {
        this.data = data;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

}
