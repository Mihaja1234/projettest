package Frame;

import comptabilite.Balance;
import java.lang.reflect.Field;
import javax.swing.JFrame;

public class GenerateFrame extends JFrame {

    ButtonEcouteur[] button;
    Input[] input;

    public ButtonEcouteur[] getButton() {
        return button;
    }

    public void setButton(ButtonEcouteur[] button) {
        this.button = button;
    }

    public Input[] getInput() {
        return input;
    }

    public void setInput(Input[] input) {
        this.input = input;
    }

    public Input getInput(int i) {
        return input[i];
    }

    public void generateFormulaire(Object o, int x, int y) {
        fillInput(o, x, y);
        fillButton();
    }
    
    public void fillInput(Object o, int x, int y){
        
        Field[] att = o.getClass().getDeclaredFields();
        input = new Input[att.length];
        for (int i = 0; i < att.length; i++) {
            y = y + 30;
            input[i] = new Input(this, att[i].getName(), x, y);
        }
    }
    
    public void fillButton(){
        button = new ButtonEcouteur[2];
        button[0] = new ButtonEcouteur(this, 120, input[input.length - 1].getTextfield().getY() + 50);
        button[0].setText("ENREGISTRER");
    }

    public void generateFormulaire(Object o) {
        generateFormulaire(o, 100, 50);
    }

    public GenerateFrame() {
        setBounds(0, 0, 1000, 800);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String args[]) {
        GenerateFrame f = new GenerateFrame();
        f.generateFormulaire(new Balance());
        f.setVisible(true);
    }
}
