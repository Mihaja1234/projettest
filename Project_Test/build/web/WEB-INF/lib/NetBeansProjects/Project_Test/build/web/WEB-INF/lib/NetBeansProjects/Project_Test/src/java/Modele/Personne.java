/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

import connexion.Connexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class Personne {

    public String nom;
    public String prenom;

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }
    
    public Personne(){
        
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void save() {
        Connexion con;
        try {
            con = new Connexion();
            PreparedStatement pstat = con.getPreparedStatement("insert into personne (nom,prenom) values (?,?)");
            pstat.setString(1, this.nom);
            pstat.setString(2, this.prenom);
            int res = pstat.executeUpdate();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Personne.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static ArrayList<Personne> getAll() {
        Connexion con;
        ArrayList<Personne> liste = new ArrayList();
        try {
            con = new Connexion();
            PreparedStatement pstat = con.getPreparedStatement("select * from personne");
            ResultSet res = con.getResultSet();
            while (res.next()) {                
                liste.add(new Personne(res.getString("nom"),res.getString("prenom")));
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Personne.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }
    

}
