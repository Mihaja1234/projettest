/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

import connexion.Connexion;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MajoorNekena
 */
public class Departement {
    public int id;
    public String nom;
    public Date date;

    public Departement(int id, String nom, Date date) {
        this.id = id;
        this.nom = nom;
        this.date = date;
    }

    public Departement() {
      
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
        public void save() {
        Connexion con;
        try {
            con = new Connexion();
            PreparedStatement pstat = con.getPreparedStatement("insert into departement (id,nom,date) values (?,?,?)");
            pstat.setInt(1, this.id);
            pstat.setString(2, this.nom);
            pstat.setDate(3, this.date);
            int res = pstat.executeUpdate();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Personne.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static ArrayList<Departement> getAll() {
        Connexion con;
        ArrayList<Departement> liste = new ArrayList();
        try {
            con = new Connexion();
            PreparedStatement pstat = con.getPreparedStatement("select * from departement");
            ResultSet res = con.getResultSet();
            while (res.next()) {                
                liste.add(new Departement(res.getInt("id"),res.getString("nom"),res.getDate("date")));
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Personne.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }
    
}
