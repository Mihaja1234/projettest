/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Annotation.UrlAnnotation;
import Modele.Departement;
import Modele.Personne;
import java.util.ArrayList;
import java.util.HashMap;
import mv.ModelView;


public class TestController {
    
    @UrlAnnotation.url(name="liste",methode="liste")
    public ModelView liste() throws Exception{
        HashMap<String,Object> val = new HashMap();
        ArrayList<Personne> array = Personne.getAll();
        val.put("liste", array);
        ModelView vm = new ModelView(val,"liste");
        return vm;
    }
    
     @UrlAnnotation.url(name="liste2",methode="liste2")
    public ModelView liste2() throws Exception{
        HashMap<String,Object> val = new HashMap();
        ArrayList<Personne> obje = Personne.getAll();
        val.put("liste",obje);
        ModelView vm = new ModelView(val,"liste");
        return vm;
    }
    
       @UrlAnnotation.url(name="liste_departement",methode="liste_departement")
    public ModelView liste_departement() throws Exception{
        HashMap<String,Object> val = new HashMap();
        ArrayList<Departement> obje = Departement.getAll();
        val.put("liste",obje);
        ModelView vm = new ModelView(val,"liste_departement");
        return vm;
    }
}
